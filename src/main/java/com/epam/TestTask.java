package com.epam;

import com.google.api.services.bigquery.model.TableRow;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.AvroIO;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.*;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.*;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestTask {

    public static class FormKVFromTableRow extends SimpleFunction<TableRow, Integer> {
        @Override
        public Integer apply(TableRow r) {
            return (Integer) r.get("age");
        }
    }

    public static Schema schemaReader(String schemaFileName) {
        Schema schema = null;
        try {
            schema = new Schema.Parser().parse(new File(schemaFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return schema;
    }

    static void runTestTask(TestTaskOptions options) {

        Integer ageFilter = options.getAgeFilter();
        String emailFilter = options.getGmailFilter();
        Pipeline p = Pipeline.create(options);
        Schema schemaAgeString = schemaReader(options.getAgeStringSchemaFile());
        Schema schemaUsers = schemaReader(options.getUsersSchemaFile());

        PCollection<TableRow> ageStringTable =
                p.apply(AvroIO.readGenericRecords(schemaAgeString).from(options.getAgeStringFile()))
                        .apply(ParDo.of(new DoFn<GenericRecord, TableRow>() {
                            @DoFn.ProcessElement
                            public void processElement(ProcessContext c) {
                                GenericRecord row = c.element();
                                Integer age = Integer.parseInt(row.get("age").toString());
                                String ageString = row.get("ageString").toString();
                                TableRow outRow = new TableRow().set("age", age).set("ageString", ageString);
                                c.output(outRow);
                            }
                        }));

        TupleTag<TableRow> usersFilteredTag = new TupleTag<TableRow>() {
        };
        TupleTag<TableRow> userErrorTag = new TupleTag<TableRow>() {
        };

        PCollectionTuple userFilterResults =
                p.apply(AvroIO.readGenericRecords(schemaUsers).from(options.getUsersFile()))
                        .apply(ParDo.of(new DoFn<GenericRecord, TableRow>() {
                            @DoFn.ProcessElement
                            public void processElement(ProcessContext c) {
                                GenericRecord row = c.element();
                                String email = row.get("email").toString();
                                Integer age = Integer.parseInt(row.get("age").toString());
                                String userName = row.get("userName").toString();
                                TableRow user = new TableRow().set("age", age).set("email", email).set("userName", userName);
                                c.output(user);
                            }
                        }))
                        .apply(ParDo.of(new DoFn<TableRow, TableRow>() {

                            @DoFn.ProcessElement
                            public void processElement(ProcessContext c, MultiOutputReceiver out) {
                                TableRow row = c.element();
                                Integer age = (Integer) row.get("age");
                                String email = row.get("email").toString();
                                Boolean error = false;
                                if (age < ageFilter) {
                                    error = true;
                                    out.get(userErrorTag).output(row.clone().set("error", "age < " + ageFilter));
                                }
                                if (!email.toLowerCase().contains(emailFilter.toLowerCase())) {
                                    error = true;
                                    out.get(userErrorTag).output(row.clone().set("error", "email not in " + emailFilter + " domain"));
                                }
                                if (!error) {
                                    out.get(usersFilteredTag).output(row);
                                }
                            }
                        }).withOutputTags(usersFilteredTag,
                                TupleTagList.of(userErrorTag)));

        PCollection<TableRow> usersFiltered =
                userFilterResults.get(usersFilteredTag)
                        .apply(ParDo.of(new DoFn<TableRow, TableRow>() {
                            @ProcessElement
                            public void processElement(ProcessContext c) {
                                TableRow row = c.element();
                                Pattern pattern = Pattern.compile("^(.*?)\\@");
                                String email = row.get("email").toString();
                                Matcher matcher = pattern.matcher(email);
                                if (matcher.find()) c.output(row.clone().set("shortUserName", matcher.group(1)));
                            }
                        }));

        PCollection<TableRow> usersToError =
                userFilterResults.get(userErrorTag);

        usersToError.apply(MapElements.via(new SimpleFunction<TableRow, String>() {
            @Override
            public String apply(TableRow row) {
                String res = row.get("age") + ", " + row.get("email") + ", " + row.get("userName") + ", " + row.get("error");
                System.out.println(res);
                return res;

            }
        })).apply("WriteResult", TextIO.write().withHeader("age, email, userName, error").withoutSharding().withSuffix(".csv").to(options.getErrorOutput()));

        PCollection<KV<Integer, TableRow>> usersGmailFinalKV = usersFiltered.apply(WithKeys.of(new FormKVFromTableRow()));
        PCollection<KV<Integer, TableRow>> ageStringTableKV = ageStringTable.apply(WithKeys.of(new FormKVFromTableRow()));

        PCollectionView<Map<Integer, TableRow>> ageStringSideInput = ageStringTableKV.apply(View.asMap());
        PCollection<KV<Integer, KV<TableRow, TableRow>>> output = usersGmailFinalKV.apply(ParDo.of(new DoFn<KV<Integer, TableRow>, KV<Integer, KV<TableRow, TableRow>>>() {
            @ProcessElement
            public void processElement(ProcessContext context) {
                Map<Integer, TableRow> ageStringByAge = context.sideInput(ageStringSideInput);
                KV<Integer, TableRow> processedElement = context.element();
                Integer age = processedElement.getKey();
                TableRow ageString = ageStringByAge.getOrDefault(age, new TableRow().set("ageString", "unknown"));
                KV<Integer, KV<TableRow, TableRow>> v = KV.of(age, KV.of(processedElement.getValue(), ageString));
                context.output(v);
            }
        }).withSideInputs(ageStringSideInput));

        output.apply(MapElements.via(new SimpleFunction<KV<Integer, KV<TableRow, TableRow>>, String>() {
            @Override
            public String apply(KV<Integer, KV<TableRow, TableRow>> row) {
                String res = row.getKey().toString()
                        + ", " + row.getValue().getKey().get("email")
                        + ", " + row.getValue().getKey().get("userName")
                        + ", " + row.getValue().getKey().get("shortUserName")
                        + ", " + row.getValue().getValue().get("ageString");
                System.out.println(res);
                return res;

            }
        })).apply(TextIO.write().withHeader("age, email, userName, shortUserName, ageString").withoutSharding().withSuffix(".csv").to(options.getOutput()));

        p.run().waitUntilFinish();
    }

    public static void main(String[] args) {
        TestTaskOptions options =
                PipelineOptionsFactory.fromArgs(args).withValidation().as(TestTaskOptions.class);
        runTestTask(options);
    }
}