package com.epam;

import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.PipelineOptions;

public interface TestTaskOptions extends PipelineOptions {

    @Default.String("users.avro")
    String getUsersFile();

    void setUsersFile(String value);

    @Default.String("users.avsc")
    String getUsersSchemaFile();

    void setUsersSchemaFile(String value);

    @Default.String("ageString.avro")
    String getAgeStringFile();

    void setAgeStringFile(String value);

    @Default.String("ageString.avsc")
    String getAgeStringSchemaFile();

    void setAgeStringSchemaFile(String value);

    @Default.Integer(20)
    Integer getAgeFilter();

    void setAgeFilter(Integer value);

    @Default.String("Gmail")
    String getGmailFilter();

    void setGmailFilter(String value);

    @Default.String("result")
    String getOutput();

    void setOutput(String value);

    @Default.String("errors")
    String getErrorOutput();

    void setErrorOutput(String value);
}
